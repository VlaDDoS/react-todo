import React, { useEffect, useState } from 'react';
import { RouteComponent } from './components/RouteComponent/RouteComponent';
import { CustomNavbar } from './components/UI/Navbar/CustomNavbar';
import { AuthContext } from './context';

import './styles/App.css';

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if(localStorage.getItem('auth')) {
      setIsAuth(true)
    }
    setIsLoading(false);
  }, [])

  return (
    <AuthContext.Provider value={{
      isAuth,
      setIsAuth,
      isLoading
    }}>
      <div className="App">
        <CustomNavbar />
        <RouteComponent />
      </div>
    </AuthContext.Provider>
  );
}

export default App;
