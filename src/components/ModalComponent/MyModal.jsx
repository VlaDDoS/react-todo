import React from 'react';

import classes from './MyModal.module.css';

export const MyModal = ({ children, isVisible, setVisible }) => {
  const mainClasses = [classes.myModal];

  if (isVisible) {
    mainClasses.push(classes.active);
  }

  return (
    <div
      className={mainClasses.join(' ')}
      onClick={() => setVisible((isVisible = false))}
    >
      <div
        className={classes.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        {children}
      </div>
    </div>
  );
};
