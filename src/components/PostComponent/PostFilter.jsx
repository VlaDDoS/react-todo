import React from 'react';
import { CustomInput } from '../UI/Input/CustomInput';
import { CustomSelect } from '../UI/Select/CustomSelect';

export const PostFilter = ({ filter, setFilter }) => {
  const { sort, query } = filter;
  const sortedList = [
    { value: 'title', name: 'По названию' },
    { value: 'body', name: 'По описанию' },
  ]

  return (
    <div>
      <CustomInput
        value={query}
        onChange={(e) => 
          setFilter({ ...filter, query: e.target.value })
        }
        placeholder="serch..."
      />
      <CustomSelect
        value={sort}
        onChange={(selectedSort) =>
          setFilter({ ...filter, sort: selectedSort })
        }
        defaultValue={'Сортировка'}
        options={sortedList}
      />
    </div>
  );
};
