import { React, useState } from 'react';

import { CustomButton } from '../UI/Button/CustomButton';
import { CustomInput } from '../UI/Input/CustomInput';

export const PostForm = ({ create }) => {
  const [post, setPost] = useState({ title: '', body: '' });

  const addNewPost = (e) => {
    e.preventDefault();
    const newPost = { ...post, id: Date.now() };

    create(newPost);
    setPost({ title: '', body: '' });
  };

  const { title, body } = post;

  return (
    <form style={{ display: 'grid' }} onSubmit={addNewPost}>
      <label htmlFor="title">
        Заголовок
        <CustomInput
          value={title}
          onChange={(e) => setPost({ ...post, title: e.target.value })}
          type="text"
          id="title"
          name="title"
          placeholder="example title"
        />
      </label>

      <label htmlFor="body">
        Содержание
        <CustomInput
          value={body}
          onChange={(e) => setPost({ ...post, body: e.target.value })}
          type="text"
          id="body"
          name="body"
          placeholder="some text"
        />
      </label>

      <CustomButton>Запостить</CustomButton>
    </form>
  );
};
