import React from 'react';
import { useNavigate } from 'react-router-dom';
import { CustomButton } from '../UI/Button/CustomButton';

export const PostItem = ({ post, remove }) => {
  const { id, title, body } = post;
  const route = useNavigate();

  return (
    <div className="post">
      <div className="post__preview">
        <h4>
          {id}. {title}
        </h4>
        <p>{body}</p>
      </div>
      <div className="post__btns">
        <CustomButton onClick={() => route(`/posts/${id}`)}>Открыть</CustomButton>
        <CustomButton onClick={() => remove(post)}>Удалить</CustomButton>
      </div>
    </div>
  );
};
