import React from 'react';
import { CSSTransition } from 'react-transition-group';
import { TransitionGroup } from 'react-transition-group';

import { PostItem } from './PostItem';

export const PostList = ({ items, title, remove }) => {
  if (!items.length) {
    return <h1 className="header">Посты не найдены</h1>;
  }

  return (
    <div>
      <h1 className="header">{title}</h1>

      <TransitionGroup>
        {items.map((item, idx) => (
          <CSSTransition key={item.id} timeout={500} classNames="post">
            <PostItem post={item} remove={remove} />
          </CSSTransition>
        ))}
      </TransitionGroup>
    </div>
  );
};
