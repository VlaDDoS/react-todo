import React from 'react';

import classes from './CustomButton.module.css';
export const CustomButton = ({ children, ...props }) => {
  return (
    <button {...props} type="submit" className={classes.myBtn}>
      {children}
    </button>
  );
};
