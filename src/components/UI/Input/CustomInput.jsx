import React from 'react';

import classes from './CustomInput.module.css';
export const CustomInput = (props) => {
  return <input {...props} className={classes.myInput} />;
};
