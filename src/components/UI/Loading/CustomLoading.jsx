import React from 'react';

import classes from './CustomLoading.module.css';

export const CustomLoading = () => {
  return (
    <div className={classes.loadingWrapper}>
      <div className={classes.myLoading}></div>
      <p className={classes.textLoading}>Загрузка...</p>
    </div>
  );
};
