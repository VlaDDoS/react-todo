import React, { useContext } from 'react';

import classes from './CustomNavbar.module.css';

import { Link } from 'react-router-dom';
import { About } from '../../../pages/About';
import { Posts } from '../../../pages/Posts';
import { CustomButton } from '../Button/CustomButton';
import { AuthContext } from '../../../context';

export const CustomNavbar = () => {
  const { navbar, navbar__menu } = classes;
  const { isAuth, setIsAuth } = useContext(AuthContext);

  const logout = () => {
    setIsAuth(false);
    localStorage.removeItem('auth');
  };

  return (
    <nav className={navbar}>
      <CustomButton onClick={logout}>Выйти</CustomButton>
      <ul className={navbar__menu}>
        <li>
          <Link to="/posts" element={<Posts />}>
            Посты
          </Link>
        </li>
        <li>
          <Link to="/about" element={<About />}>
            О сайте
          </Link>
        </li>
      </ul>
    </nav>
  );
};
