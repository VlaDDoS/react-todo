import React from 'react';
import { usePagination } from '../../../hooks/usePagination';

import classes from './CustomPagination.module.css';

export const CustomPagination = ({ totalPages, page, changePage }) => {
  const pagesArray = usePagination(totalPages);
  const { page__btn, current__page, page__wrapper } = classes;

  return (
    <div className={page__wrapper}>
      {pagesArray.map((p) => (
        <span
          key={p}
          onClick={() => changePage(p)}
          className={page === p ? `${page__btn} ${current__page}` : page__btn}
        >
          {p}
        </span>
      ))}
    </div>
  );
};
