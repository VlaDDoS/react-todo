import React from 'react'

export const Error = () => {
  return (
    <h1 className='header error'>Такой страницы не существует</h1>
  )
}
