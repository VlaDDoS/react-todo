import React, { useContext } from 'react';
import { CustomButton } from '../components/UI/Button/CustomButton';

import { CustomInput } from '../components/UI/Input/CustomInput';
import { AuthContext } from '../context';

export const Login = () => {
  const {isAuth, setIsAuth} = useContext(AuthContext);

  const login = e => {
    e.preventDefault();
    setIsAuth(true);
    localStorage.setItem('auth', 'true');
  }

  return (
    <div>
      <h1 className="header mt30">
        Страница входа
      </h1>
      <form onSubmit={login}>
        <label htmlFor="login">
          Логин
          <CustomInput placeholder="Введите логин" />
        </label>

        <label htmlFor="password">
          Пароль
          <CustomInput placeholder="Введите пароль" />
        </label>

        <CustomButton>Войти</CustomButton>
      </form>
    </div>
  );
};
