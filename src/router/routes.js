import { About } from '../pages/About';
import { Login } from '../pages/Login';
import { PostIdPage } from '../pages/PostIdPage';
import { Posts } from '../pages/Posts';

export const privateRoutes = [
  { path: '/', element: Posts },
  { path: '/posts', element: Posts },
  { path: '/posts/:id', element: PostIdPage },
  { path: '/about', element: About },
  { path: '*', element: Posts },
];

export const publicRoutes = [
  { path: '/login', element: Login },
  { path: '*', element: Login },
];
